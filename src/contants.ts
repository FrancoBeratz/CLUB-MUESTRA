export const options = {
  actionsCellStyle: {
    backgroundColor: "#fadb5761",
    color: "#000",
  },
  actionsColumnIndex: -1,
  headerStyle: {
    backgroundColor: "rgb(3 101 36)",
    color: "white",
  },
}
export const localization = {
  toolbar: {
    searchPlaceholder: "Buscar",
  },
  header: {
    actions: "Accion",
  },
  pagination: {
    firstTooltip: "Primera Página",
    previousTooltip: "Página Anterior",
    nextTooltip: "Página Siguiente",
    lastTooltip: "Última Página",
    labelRowsSelect: "Filas",
  },
}

export const paginationOption = {
  rowsPerPageText: "Filas por Página:",
  rangeSeparatorText: "de",
  noRowsPerPage: false,
  selectAllRowsItem: false,
  selectAllRowsItemText: "Todos",
}

export const selectPlayer = [
  {
    value: "Fútbol Mayor",
    label: "Fútbol Mayor",
    isDisabled: true,
    key: "1",
  },
  {
    value: "Veteranos(FM)",
    label: "Veteranos(FM)",
    isDisabled: false,
    key: "2",
  },
  {
    value: "Reserva(FM)",
    label: "Reserva(FM)",
    isDisabled: false,
    key: "3",
  },
  {
    value: "Primera(FM)",
    label: "Primera(FM)",
    isDisabled: false,
    key: "4",
  },
  {
    value: "Fútbol Feminino",
    label: "Fútbol Feminino",
    isDisabled: true,
    key: "5",
  },
  {
    value: "Mayores(F)",
    label: "Mayores(F)",
    isDisabled: false,
    key: "6",
  },
  {
    value: "Menores(F)",
    label: "Menores(F)",
    isDisabled: false,
    key: "7",
  },
  {
    value: "Fútbol Infantil",
    label: "Fútbol Infantil",
    isDisabled: true,
    key: "8",
  },
  {
    value: "Escuelita(FI)",
    label: "Escuelita(FI)",
    isDisabled: false,
    key: "9",
  },
  {
    value: "Octava(FI)",
    label: "Octava(FI)",
    isDisabled: false,
    key: "10",
  },
  {
    value: "Septima(FI)",
    label: "Septima(FI)",
    isDisabled: false,
    key: "11",
  },
  {
    value: "Sexta(FI)",
    label: "Sexta(FI)",
    isDisabled: false,
    key: "12",
  },
  {
    value: "Quinta(FI)",
    label: "Quinta(FI)",
    isDisabled: false,
    key: "13",
  },
  {
    value: "Cuarta(FI)",
    label: "Cuarta(FI)",
    isDisabled: false,
    key: "14",
  },
]
