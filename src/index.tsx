import React from "react"
import ReactDOM from "react-dom"
import { App } from "./App"
import "./index.css"
import reportWebVitals from "./reportWebVitals"
import { createBrowserHistory } from "history"
import { Provider } from "react-redux"
import store from "./redux/store"

const history: any = createBrowserHistory()
ReactDOM.render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,
  document.getElementById("root")
)

reportWebVitals()
