import axios from "axios"
import { TypesMembers } from "../contants/action-types"

export const setMembers = (members) => {
  return {
    type: TypesMembers.SET_MEMBERS,
    payload: members,
  }
}

export const setFilterMembers = (members) => {
  return {
    type: TypesMembers.SET_MEMBERS_FILTER,
    payload: members,
  }
}

export const setAddMember = () => {
  return {
    type: TypesMembers.ADD_MEMBERS,
  }
}

export const setUpdateMember = () => {
  return {
    type: TypesMembers.UPDATE_MEMBERS,
  }
}

export const setUpdateMemberPayment = () => {
  return {
    type: TypesMembers.UPDATE_MEMBERS_PAYMENT,
  }
}

export const setRemoveMember = () => {
  return {
    type: TypesMembers.REMOVE_SELECTED_MEMBERS,
  }
}

export const getMembers = () => {
  return function (dispatch) {
    axios
      .get("http://localhost:3001/members")
      .then((response) => {
        console.log(response.data.data)
        dispatch(setMembers(response.data.data))
      })
      .catch((error) => console.error(error))
  }
}

export const addMember = (members) => {
  return function (dispatch) {
    axios
      .post("http://localhost:3001/members/", members)
      .then((response) => {
        if (response.data.message === "1") {
          dispatch(setAddMember())
          dispatch(getMembers())
        }
      })
      .catch((error) => console.error(error))
  }
}

export const updateMember = (id, member) => {
  return function (dispatch) {
    axios
      .put("http://localhost:3001/members/" + id.idSocio, member)
      .then((response) => {
        if (response.data.message === "1") {
          dispatch(setUpdateMember())
          dispatch(getMembers())
        }
      })
      .catch((error) => console.error(error))
  }
}

export const updateMemberPayment = (id, member) => {
  return function (dispatch) {
    axios
      .put("http://localhost:3001/members/update_payment/" + id.idSocio, member)
      .then((response) => {
        if (response.data.message === "1") {
          dispatch(setUpdateMemberPayment())
          dispatch(getMembers())
        }
      })
      .catch((error) => console.error(error))
  }
}

export const removeMember = (id) => {
  return function (dispatch) {
    axios
      .delete("http://localhost:3001/members/" + id)
      .then((response) => {
        dispatch(setRemoveMember())
        dispatch(getMembers())
      })
      .catch((error) => console.error(error))
  }
}

export const getFilterMembers = (member) => {
  let ruta = ""
  if (member.estadoCuota === "1") {
    ruta = "http://localhost:3001/members/membersStateOk"
  } else if (member.estadoCuota === "2") {
    ruta = "http://localhost:3001/members/membersStateError"
  }
  return function (dispatch) {
    axios
      .post(ruta, member)
      .then((response) => {
        if (response.data.data.length > 0) {
          dispatch(setFilterMembers(response.data.data))
        }
      })
      .catch((error) => console.error(error))
  }
}
