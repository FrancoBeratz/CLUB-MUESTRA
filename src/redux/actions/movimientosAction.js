import axios from "axios"
import { TypesMovimientos } from "../contants/action-types"

export const setMovimientosEgresos = (egresos) => {
  return {
    type: TypesMovimientos.SET_MOVIMIENTOS_EGRESOS,
    payload: egresos,
  }
}

export const setMovimientosIngresos = (ingresos) => {
  return {
    type: TypesMovimientos.SET_MOVIMIENTOS_INGRESOS,
    payload: ingresos,
  }
}

export const setAddMovimiento = () => {
  return {
    type: TypesMovimientos.ADD_MOVIMIENTO,
  }
}

export const setUpdateMovimiento = () => {
  return {
    type: TypesMovimientos.UPDATE_MOVIMIENTO,
  }
}

export const setRemoveMovimiento = () => {
  return {
    type: TypesMovimientos.REMOVE_SELECTED_MOVIMIENTO,
  }
}

export const setFilterMov = (movimientos) => {
  return {
    type: TypesMovimientos.SET_MOVIMIENTO_FILTER,
    payload: movimientos,
  }
}

export const setAddSumaIngreso = (ingreso) => {
  if (ingreso == null) {
    ingreso = 0
  }
  return {
    type: TypesMovimientos.ADD_SALDO_INGRESOS,
    payload: ingreso,
  }
}

export const setAddSumaEgreso = (egreso) => {
  if (egreso == null) {
    egreso = 0
  }
  return {
    type: TypesMovimientos.ADD_SALDO_EGRESOS,
    payload: egreso,
  }
}

export const setAddSumaDiferencia = (diferencia) => {
  if (diferencia == null) {
    diferencia = 0
  }
  return {
    type: TypesMovimientos.ADD_SALDO_DIFERENCIA,
    payload: diferencia,
  }
}

export const getMovimientosEgresos = () => {
  return function (dispatch) {
    axios
      .get("http://localhost:3001/movimientos/egresos")
      .then((response) => {
        console.log(response.data.data)
        dispatch(setMovimientosEgresos(response.data.data))
      })
      .catch((error) => console.error(error))
  }
}

export const getMovimientosIngresos = () => {
  return function (dispatch) {
    axios
      .get("http://localhost:3001/movimientos/ingresos")
      .then((response) => {
        console.log(response.data.data)
        dispatch(setMovimientosIngresos(response.data.data))
      })
      .catch((error) => console.error(error))
  }
}

export const addMovimiento = (movimientos) => {
  return function (dispatch) {
    axios
      .post("http://localhost:3001/movimientos/", movimientos)
      .then((response) => {
        console.log("addd", response)
        if (response.data.message === "1") {
          dispatch(setAddMovimiento())
          // eslint-disable-next-line @typescript-eslint/no-use-before-define
          dispatch(getFilterMovimiento(movimientos))
        }
      })
      .catch((error) => console.error(error))
  }
}

export const updateMovimiento = (id, movimiento) => {
  return function (dispatch) {
    axios
      .put("http://localhost:3001/movimientos/" + id.idMov, movimiento)
      .then((response) => {
        if (response.data.message === "1") {
          dispatch(setUpdateMovimiento())
          // eslint-disable-next-line @typescript-eslint/no-use-before-define
          dispatch(getFilterMovimiento(movimiento))
        }
      })
      .catch((error) => console.error(error))
  }
}

export const removeMovimiento = (id) => {
  return function (dispatch) {
    axios
      .delete("http://localhost:3001/movimientos/" + id)
      .then((response) => {
        dispatch(setRemoveMovimiento())
        dispatch(getMovimientosIngresos())
        dispatch(getMovimientosEgresos())
      })
      .catch((error) => console.error(error))
  }
}

export const getFilterMovimiento = (movimiento) => {
  return function (dispatch) {
    axios
      .post("http://localhost:3001/movimientos/searchMov", movimiento)
      .then((response) => {
        const diferencia =
          response.data.totalIngresos[0].total_ingreso -
          response.data.totalEgreso[0].total_egreso
        dispatch(setMovimientosIngresos(response.data.data))
        dispatch(setMovimientosEgresos(response.data.dataEgreso))
        dispatch(
          setAddSumaIngreso(response.data.totalIngresos[0].total_ingreso)
        )
        dispatch(setAddSumaEgreso(response.data.totalEgreso[0].total_egreso))
        dispatch(setAddSumaDiferencia(diferencia))
      })
      .catch((error) => console.error(error))
  }
}
