import {
  ActionTypes,
  TypesMembers,
  TypesMovimientos,
} from "../contants/action-types"

const initialState = {
  players: [],
  members: [],
  ingresos: [],
  egresos: [],
  sumaIngresos: 0,
  sumaEgresos: 0,
  diferenciaSaldo: 0,
}

export const playersReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_PLAYERS:
      return { ...state, players: payload }
    case ActionTypes.REMOVE_SELECTED_PLAYER:
      return { ...state }
    case ActionTypes.ADD_PLAYER:
      return { ...state }
    case ActionTypes.UPDATE_PLAYER:
      return { ...state, players: payload }
    default:
      return state
  }
}

export const membersReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TypesMembers.SET_MEMBERS:
      return { ...state, members: payload }
    case TypesMembers.SET_MEMBERS_FILTER:
      return { ...state, members: payload }
    case TypesMembers.REMOVE_SELECTED_MEMBERS:
      return { ...state }
    case TypesMembers.ADD_MEMBERS:
      return { ...state }
    case TypesMembers.UPDATE_MEMBERS:
      return { ...state, members: payload }
    case TypesMembers.UPDATE_MEMBERS_PAYMENT:
      return { ...state, members: payload }
    default:
      return state
  }
}

export const movimientosReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TypesMovimientos.SET_MOVIMIENTOS_EGRESOS:
      return { ...state, egresos: payload }
    case TypesMovimientos.SET_MOVIMIENTOS_INGRESOS:
      return { ...state, ingresos: payload }
    case TypesMovimientos.REMOVE_SELECTED_MOVIMIENTO:
      return { ...state }
    case TypesMovimientos.ADD_MOVIMIENTO:
      return { ...state }
    case TypesMovimientos.UPDATE_MOVIMIENTO:
      return { ...state, movimientos: payload }
    case TypesMovimientos.ADD_SALDO_INGRESOS:
      return { ...state, sumaIngresos: payload }
    case TypesMovimientos.ADD_SALDO_EGRESOS:
      return { ...state, sumaEgresos: payload }
    case TypesMovimientos.ADD_SALDO_DIFERENCIA:
      return { ...state, diferenciaSaldo: payload }
    default:
      return state
  }
}
