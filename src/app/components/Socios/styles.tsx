import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: "100%",
      },
    },
    principal: {
      display: "flex",
      justifyContent: "center",
      minHeight: "500px",
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    formOk: {
      marginTop: "15px",
      marginLeft: "8px",
      display: "flex",
      justifyContent: "center",
      color: "white",
      background: "green",
      textAlign: "center",
      borderRadius: "5px",
    },
    paper: {
      width: "90%",
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    containerEliminar: {
      background: "#fff",
      padding: "24px",
      width: "auto",
      minWidth: "600px",
      borderRadius: "5px",
    },
    gridHeader: {
      background: "#ffcc00a8",
      margin: "-35px 15px 0",
      borderRadius: "3px",
      padding: "5px",
      "& > h2": {
        color: "#000",
      },
    },
    tableContainer: {
      marginTop: "30px",
      "& :nth-child(6)": {
        display: "none",
      },
    },
    input: {
      height: "40px",
      padding: "10px",
      borderRadius: "8px",
      border: "none",
      boxShadow: "1px 1px 3.8px #0c1d2e",
      marginTop: "10px",
      marginBottom: "14px",
    },
    paperModal: {
      borderRadius: "8px",
      width: "40%",
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      "& > h2": {
        textAlign: "center",
      },
    },
    lblForm: {
      textAlign: "start",
    },
    footerModal: {
      marginTop: "45px",
      textAlign: "center",
    },
    btnModal: {
      width: "80%",
      height: "50px",
    },
    btnAddUser: {
      margin: theme.spacing(2),
      padding: "18px",
      width: "30%",
      marginTop: "20px",
    },
    rootPdf: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    cardPdf: {
      width: "800px",
      height: "auto",
    },
    titlePdf: {
      textAlign: "center",
      fontSize: "35px",
    },
    bodyPdf: {
      margin: "30px 40px",
    },
    contpdf: {
      display: "flex",
    },
    borderPdf: {
      border: "1px solid #000",
      margin: "12px 20px",
    },
    firmaPdf: {
      display: "flex",
      justifyContent: "flex-end",
      marginTop: "30px",
      "& > div": {
        textAlign: "center",
      },
    },
    rootCard: {
      flexGrow: 1,
      marginTop: "40px",
      background: "#fff",
      boxShadow: "1px 1px 4px 1px #ddd",
      "& > h2": {
        color: "#000",
        textAlign: "start",
        paddingLeft: "35px",
      },
    },
    filterDate: {
      width: "90%",
    },
    filterCard: {
      display: "flex",
      alignItems: "center",
      margin: "20px 0",
    },
    btnTable: {
      margin: "7px",
      height: "65%",
      justifyContent: "end",
    },
    sendRta: {
      justifyContent: "center",
    },
  })
)
