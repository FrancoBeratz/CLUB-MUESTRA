import React from "react"
import { useStyles } from "./styles"
import Card from "@material-ui/core/Card"

const pdf = (props: any) => {
  console.log(props)
  const classes = useStyles()

  return (
    <div className={classes.rootPdf} id="rootPdf">
      <div className="html2pdf__page-break">
        <Card className={classes.cardPdf}>
          <div className={classes.borderPdf}>
            <p className={classes.titlePdf}>Recibo</p>
            <div className={classes.bodyPdf}>
              <div className={classes.contpdf}>
                <p>Recibi de: {props.nombre}</p>
                <p>
                  .........................................................................................
                </p>
              </div>
              <div className={classes.contpdf}>
                <p>La cantidad de: {props.importe}</p>
                <p>
                  .........................................................................................
                </p>
              </div>
              <div className={classes.contpdf}>
                <p>Por concepto de: {props.nombre}</p>
                <p>
                  .........................................................................................
                </p>
              </div>
              <div className={classes.firmaPdf}>
                <div>
                  <p>_______________________</p>
                  <p>Firma y Aclaración</p>
                </div>
              </div>
            </div>
          </div>
        </Card>
      </div>
    </div>
  )
}

export default pdf
