import React from "react"
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  PDFViewer,
} from "@react-pdf/renderer"

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    height: "auto",
    width: "800px",
  },
  pdfViewer: {
    height: "800px",
    width: "800px",
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  rootPdf: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  cardPdf: {
    width: "800px",
    height: "auto",
  },
  titlePdf: {
    textAlign: "center",
    fontSize: "35px",
  },
  bodyPdf: {
    margin: "10px 40px",
  },
  contpdf: {
    display: "flex",
    marginTop: "20px",
  },
  borderPdf: {
    border: "1px solid #000",
    margin: "12px 20px",
  },
  firmaPdf: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: "30px",
  },
  firmaPdfContainer: {
    textAlign: "right",
  },
  firmaPdfContainerText: {
    textAlign: "right",
    marginRight: "35px",
    fontSize: "15px",
  },
  title: {
    margin: "10px 10px",
    fontSize: "25px",
    textAlign: "center",
  },
  fechaPago: {
    display: "flex",
    fontSize: "15px",
    margin: "10px",
    marginTop: "35px",
    textAlign: "right",
  },
})

// Create Document Component
const MyDocument = (props: any) => {
  return (
    <PDFViewer style={styles.pdfViewer}>
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.section}>
            <View style={styles.borderPdf}>
              <Text style={styles.title}>RECIBO DE PAGO</Text>
              <Text style={styles.fechaPago}>Fecha de pago: {props.fecha}</Text>
              <View style={styles.bodyPdf}>
                <View style={styles.contpdf}>
                  <Text>
                    Recibí de: {props.nombre} ............................
                  </Text>
                </View>
                <View style={styles.contpdf}>
                  <Text>
                    La cantidad de: {props.importe}{" "}
                    .............................
                  </Text>
                </View>
                <View style={styles.contpdf}>
                  <Text>
                    Por concepto de: Pago {props.concepto}{" "}
                    ........................
                    ...................................................
                  </Text>
                </View>
                <View style={styles.firmaPdf}>
                  <View>
                    <Text style={styles.firmaPdfContainer}>
                      _____________________
                    </Text>
                    <Text style={styles.firmaPdfContainerText}>
                      Firma y Aclaración
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Page>
      </Document>
    </PDFViewer>
  )
}
export default MyDocument
