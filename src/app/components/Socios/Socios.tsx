import { Grid, TextField } from "@material-ui/core"
import React, { useEffect, useState } from "react"
import Paper from "@material-ui/core/Paper"
import Modal from "@material-ui/core/Modal"
import Button from "@material-ui/core/Button"
import { useFormik } from "formik"
import AddCircleIcon from "@material-ui/icons/AddCircle"
import { useStyles } from "./styles"
import InputAdornment from "@material-ui/core/InputAdornment"
import DeleteIcon from "@material-ui/icons/Delete"
import Card from "@material-ui/core/Card"
import SearchIcon from "@material-ui/icons/Search"
import MyDocument from "./MyDocument"
import { useDispatch, useSelector } from "react-redux"
import GroupIcon from "@material-ui/icons/Group"
import {
  getMembers,
  updateMember,
  removeMember,
  addMember,
  updateMemberPayment,
  getFilterMembers,
} from "../../../redux/actions/partnerAction"
import store from "../../../redux/store"
import DataTable from "react-data-table-component"
import DataTableExtensions from "react-data-table-component-extensions"
import "react-data-table-component-extensions/dist/index.css"
import SortIcon from "@material-ui/icons/ArrowDownward"
import { paginationOption } from "../../../contants"
import EditIcon from "@material-ui/icons/Edit"
import CreditCardIcon from "@material-ui/icons/CreditCard"
import ResponsiveDrawer from "../ResponsiveDrawer"
import Alert from "@material-ui/lab/Alert"
import CheckIcon from "@material-ui/icons/Check"
import Footer from "../Footer"

export function Socios() {
  const [abrirModalAlta, setAbrirModalAlta] = useState(false)
  const [abrirModalPayment, setAbrirModalPayment] = useState(false)
  const [titleEdit, setTitleEdit] = useState(false)
  const [titleAlta, setTitleAlta] = useState(false)
  const [abrirModalEliminar, setAbrirModalEliminar] = useState(false)
  const [abrirModalPdf, setAbrirModalPdf] = useState(false)
  const [sendFrom, setSendForm] = useState(false)
  const [sendFromUpdate, setSendFromUpdate] = useState(false)
  const [socio, setSocio] = useState("")
  const [idSocio, setIdSocio] = useState("")
  const dispatch = useDispatch()
  type RootState = ReturnType<typeof store.getState>
  const members = useSelector((state: RootState) => state)
  const data: any[] = members.allMembers.members
  const classes = useStyles()
  const validate = (values: any) => {
    const errors: any = {}
    if (!values.nameLastName) {
      errors.nameLastName = "Debe ingresar un nombre"
    }
    if (!values.dni) {
      errors.dni = "Debe ingresar un dni"
    }
    return errors
  }

  let fechaFactura = ""
  let fechaObtenida = ""
  const date = new Date()
  const day = date.getDate()
  const month = date.getMonth() + 1
  const year = date.getFullYear()
  if (month < 10 && day < 10) {
    fechaObtenida = `${year}-0${month}-0${day}`
    fechaFactura = `0${day}/0${month}/${year}`
  } else if (month < 10) {
    fechaObtenida = `${year}-0${month}-${day}`
    fechaFactura = `${day}/0${month}/${year}`
  } else if (day < 10) {
    fechaObtenida = `${year}-${month}-0${day}`
    fechaFactura = `0${day}/${month}/${year}`
  } else {
    fechaObtenida = `${year}-${month}-${day}`
    fechaFactura = `${day}/${month}/${year}`
  }

  const formik = useFormik({
    initialValues: {
      nameLastName: "",
      dni: "",
      fechaNac: fechaObtenida,
      fechaAfiliacion: fechaObtenida,
      importe: "0",
      periodo: "Semestre",
      nameLastNameSocio: "",
      estadoCuota: "1",
      fechaHasta: fechaObtenida,
      fechaFin: fechaObtenida,
    },
    validate,
    onSubmit: (values, { resetForm }) => {
      console.log("subuitmii", values)
      resetForm()
      setSendForm(true)
      setTimeout(() => {
        setSendForm(false)
        dispatch(addMember(values))
        setAbrirModalAlta(!abrirModalAlta)
      }, 3000)
    },
  })

  const limpiarForm = () => {
    formik.values.nameLastName = ""
    formik.values.dni = ""
    formik.values.fechaNac = fechaObtenida
    formik.values.fechaAfiliacion = fechaObtenida
    formik.values.nameLastNameSocio = ""
  }

  const abrirCerrarModalEliminar = () => {
    setAbrirModalEliminar(!abrirModalEliminar)
  }

  const abrirCerrarModalAlta = () => {
    limpiarForm()
    setTitleEdit(false)
    setTitleAlta(true)
    setAbrirModalAlta(!abrirModalAlta)
  }

  const abrirCerrarModalPayment = () => {
    setAbrirModalPayment(!abrirModalPayment)
  }

  const seleccionarSocio = (rowData: any, caso: any) => {
    if (caso === "Editar") {
      formik.values.nameLastName = rowData.nombre_soc
      formik.values.dni = rowData.dni_soc
      const [yearNac, monthNac, dayNac] = rowData.fechaNac_soc
        ? rowData.fechaNac_soc.split("/")
        : ""
      formik.values.fechaNac = `${dayNac}-${monthNac}-${yearNac}`
      const [yearAfiliacion, monthAfiliacion, dayAfiliacion] =
        rowData.fechaAfil_soc ? rowData.fechaAfil_soc.split("/") : ""
      formik.values.fechaAfiliacion = `${dayAfiliacion}-${monthAfiliacion}-${yearAfiliacion}`
      setTitleEdit(true)
      setTitleAlta(false)
      setIdSocio(rowData.id_soc)
      setAbrirModalAlta(!abrirModalAlta)
    }
    if (caso === "Eliminar") {
      setSocio(rowData.nombre_soc)
      setIdSocio(rowData.id_soc)
      setAbrirModalEliminar(!abrirModalEliminar)
    }
  }

  const abrirModalPago = (rowData: any, caso: any) => {
    console.log("modal pago ", formik.values.importe)
    setIdSocio(rowData.id_soc)
    setSocio(rowData.nombre_soc)
    formik.values.nameLastNameSocio = rowData.nombre_soc
    setAbrirModalPayment(!abrirModalPayment)
  }

  const columns = [
    {
      name: "Nombre y Apellido",
      selector: (row) => row.nombre_soc,
      sortable: true,
    },
    {
      name: "Dni",
      selector: (row) => row.dni_soc,
      sortable: true,
    },
    {
      name: "Fecha Nacimiento",
      selector: (row) => row.fechaNac_soc,
      sortable: true,
    },
    {
      name: "Fecha Afiliación",
      selector: (row) => row.fechaAfil_soc,
      sortable: true,
    },
    {
      name: "Cuota Paga Hasta",
      selector: (row) => row.cuotaHasta_soc,
      sortable: true,
    },
    {
      name: "Estado de la cuota",
      selector: (row) => row.estadoCuota_soc,
      sortable: true,
    },
    {
      name: "Acción",
      sortable: false,
      selector: "null",
      cell: (d) => [
        <Button
          key={1}
          variant="contained"
          type="button"
          className={classes.btnTable}
          title="Editar Socio"
          startIcon={<EditIcon />}
          color="primary"
          onClick={() => seleccionarSocio(d, "Editar")}
        ></Button>,
        <Button
          key={2}
          variant="contained"
          className={classes.btnTable}
          type="button"
          color="secondary"
          title="Eliminar Socio"
          startIcon={<DeleteIcon />}
          onClick={() => seleccionarSocio(d, "Eliminar")}
        ></Button>,
        <Button
          key={3}
          variant="contained"
          className={classes.btnTable}
          type="button"
          color="default"
          title="Generar Recibo"
          startIcon={<CreditCardIcon />}
          onClick={() => abrirModalPago(d, "")}
        ></Button>,
      ],
      grow: 2,
      center: true,
    },
  ]

  const viewPayment = (e: any) => {
    dispatch(updateMemberPayment({ idSocio }, formik.values))
    setAbrirModalPayment(!abrirModalPayment)
    setAbrirModalPdf(!abrirModalPdf)
  }

  const searchStateMember = () => {
    dispatch(getFilterMembers(formik.values))
  }

  const searchAllMembers = () => {
    dispatch(getMembers())
  }

  const abrirCerrarModalPdf = () => {
    setAbrirModalPdf(!abrirModalPdf)
  }

  const update = (id: any) => {
    setSendFromUpdate(true)
    setTimeout(() => {
      dispatch(updateMember(id, formik.values))
      setAbrirModalAlta(!abrirModalAlta)
      // limpiarForm()
      setSendFromUpdate(false)
    }, 3000)
  }

  const deleteSocio = () => {
    dispatch(removeMember(idSocio))
    abrirCerrarModalEliminar()
  }

  const tableDataSocios = {
    columns,
    data,
    print: false,
    export: false,
    filterPlaceholder: "Ingrese una palabra",
  }

  useEffect(() => {
    dispatch(getMembers())
  }, [dispatch])

  const bodyAltaSocio = (
    <div className={classes.paperModal}>
      {titleEdit && <h2>Editar Socio</h2>}
      {titleAlta && <h2>Alta Socio</h2>}
      <form onSubmit={formik.handleSubmit} className={classes.root}>
        <TextField
          id="standard-basic"
          label="Nombre y Apellido"
          value={formik.values.nameLastName}
          name="nameLastName"
          onChange={formik.handleChange}
          variant="outlined"
          onBlur={formik.handleBlur}
        />
        {formik.errors.nameLastName && formik.touched.nameLastName && (
          <div className="error">{formik.errors.nameLastName}</div>
        )}
        <TextField
          id="standard-basic"
          label="Dni"
          value={formik.values.dni}
          name="dni"
          onChange={formik.handleChange}
          variant="outlined"
          type="number"
        />
        {formik.errors.dni && formik.touched.dni && (
          <div className="error">{formik.errors.dni}</div>
        )}
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <TextField
              id="standard-basic"
              label="Fecha de Nacimiento"
              value={formik.values.fechaNac}
              name="fechaNac"
              onChange={formik.handleChange}
              variant="outlined"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              id="standard-basic"
              label="Fecha de Afiliación"
              value={formik.values.fechaAfiliacion}
              name="fechaAfiliacion"
              onChange={formik.handleChange}
              variant="outlined"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
        </Grid>

        <Grid container spacing={3} className={classes.footerModal}>
          <Grid item xs={6}>
            {titleAlta && (
              <Button
                variant="contained"
                type="submit"
                color="primary"
                className={classes.btnModal}
              >
                Guardar
              </Button>
            )}
            {titleEdit && (
              <Button
                variant="contained"
                type="button"
                color="primary"
                className={classes.btnModal}
                onClick={() => update({ idSocio })}
              >
                Actualizar
              </Button>
            )}
          </Grid>
          <Grid item xs={6}>
            <Button
              variant="contained"
              onClick={abrirCerrarModalAlta}
              className={classes.btnModal}
            >
              Cancelar
            </Button>
          </Grid>
        </Grid>
        {sendFrom && (
          <Grid container className={classes.formOk}>
            <Grid item xs={12}>
              <Alert
                variant="filled"
                icon={<CheckIcon fontSize="inherit" />}
                severity="success"
                className={classes.sendRta}
              >
                Socio dado de alta con éxito
              </Alert>
            </Grid>
          </Grid>
        )}
        {sendFromUpdate && (
          <Grid container className={classes.formOk}>
            <Grid item xs={12}>
              <Alert
                variant="filled"
                icon={<CheckIcon fontSize="inherit" />}
                severity="success"
                className={classes.sendRta}
              >
                Socio actualizado correctamente
              </Alert>
            </Grid>
          </Grid>
        )}
      </form>
    </div>
  )

  const bodyPayment = (
    <div className={classes.paperModal}>
      <h2>Generar Recibo de Pago</h2>
      <form className={classes.root}>
        <TextField
          id="standard-basic"
          label="Nombre de Socio"
          value={formik.values.nameLastNameSocio}
          name="nameLastNameSocio"
          onChange={formik.handleChange}
          variant="outlined"
        />
        {formik.errors.nameLastNameSocio &&
          formik.touched.nameLastNameSocio && (
            <div className="error">{formik.errors.nameLastNameSocio}</div>
          )}

        <TextField
          id="standard-basic"
          label="Fecha Fin de Pago"
          name="fechaFin"
          value={formik.values.fechaFin}
          onChange={formik.handleChange}
          variant="outlined"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="outlined-select-currency-native"
          select
          label="Seleccionar Período"
          name="periodo"
          onChange={formik.handleChange}
          value={formik.values.periodo}
          SelectProps={{
            native: true,
          }}
          variant="outlined"
        >
          <option key="1" value="Semestre">
            Semestre
          </option>
          <option key="2" value="Anual">
            Anual
          </option>
        </TextField>
        <TextField
          id="standard-basic"
          label="Importe"
          name="importe"
          value={formik.values.importe}
          onChange={formik.handleChange}
          onKeyUp={(event) => {
            console.log(event.keyCode)
            if (event.keyCode >= 48 && event.keyCode <= 57) {
              return true
            }
            return false
          }}
          variant="outlined"
          InputProps={{
            startAdornment: <InputAdornment position="start">$</InputAdornment>,
          }}
          type="text"
        />
        <Grid container spacing={3} className={classes.footerModal}>
          <Grid item xs={6}>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              className={classes.btnModal}
              onClick={viewPayment}
            >
              Generar Recibo
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              variant="contained"
              onClick={abrirCerrarModalPayment}
              className={classes.btnModal}
            >
              Cancelar
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  )

  return (
    <>
      <ResponsiveDrawer>
        <div>
          <Grid container spacing={3}>
            <Grid item xs={12} className={classes.principal}>
              <Paper className={classes.paper}>
                <div className={classes.gridHeader}>
                  <h2>Listado de Socios del club</h2>
                </div>
                <div>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.btnAddUser}
                    onClick={abrirCerrarModalAlta}
                    startIcon={<AddCircleIcon />}
                  >
                    Añadir nuevo socio
                  </Button>
                </div>
                <Card className={classes.rootCard}>
                  <h2>Filtros de búsqueda</h2>
                  <form onSubmit={formik.handleSubmit} action="">
                    <Grid container spacing={1} className={classes.filterCard}>
                      <Grid item lg={3} md={3} sm={6}>
                        <TextField
                          id="standard-basic"
                          label="Fecha Hasta"
                          className={classes.filterDate}
                          name="fechaHasta"
                          value={formik.values.fechaHasta}
                          onChange={formik.handleChange}
                          variant="outlined"
                          type="date"
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      </Grid>
                      <Grid item lg={3} md={3} sm={12}>
                        <TextField
                          id="outlined-select-currency-native"
                          select
                          label="Estado de la cuota"
                          name="estadoCuota"
                          onChange={formik.handleChange}
                          style={{ width: "95%" }}
                          value={formik.values.estadoCuota}
                          SelectProps={{
                            native: true,
                          }}
                          variant="outlined"
                        >
                          <option key="1" value="1">
                            Cuota al día
                          </option>
                          <option key="2" value="2">
                            Deuda
                          </option>
                        </TextField>
                      </Grid>
                      <Grid item lg={3} md={3} sm={12}>
                        <Button
                          variant="contained"
                          color="primary"
                          type="submit"
                          className={classes.btnModal}
                          startIcon={<SearchIcon />}
                          onClick={searchStateMember}
                        >
                          Buscar
                        </Button>
                      </Grid>
                      <Grid item lg={3} md={3} sm={12}>
                        <Button
                          variant="contained"
                          color="primary"
                          className={classes.btnModal}
                          onClick={searchAllMembers}
                          startIcon={<GroupIcon />}
                        >
                          Listar todos los socios
                        </Button>
                      </Grid>
                    </Grid>
                  </form>
                </Card>
                <div className={classes.tableContainer}>
                  <DataTableExtensions {...tableDataSocios}>
                    <DataTable
                      columns={columns}
                      data={data}
                      sortIcon={<SortIcon />}
                      defaultSortAsc={true}
                      title="Listado de Socios"
                      pagination
                      highlightOnHover
                      dense
                      paginationComponentOptions={paginationOption}
                      noDataComponent="No hay datos para mostrar"
                    />
                  </DataTableExtensions>
                </div>
              </Paper>
            </Grid>

            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={abrirModalAlta}
              onClose={abrirCerrarModalAlta}
              BackdropProps={{
                timeout: 500,
              }}
            >
              {bodyAltaSocio}
            </Modal>

            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={abrirModalPayment}
              onClose={abrirCerrarModalPayment}
              BackdropProps={{
                timeout: 500,
              }}
            >
              {bodyPayment}
            </Modal>

            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={abrirModalEliminar}
              onClose={abrirCerrarModalEliminar}
              BackdropProps={{
                timeout: 500,
              }}
            >
              <div className={classes.containerEliminar}>
                <h2>¿Desea eliminar a {socio}?</h2>
                <Grid container spacing={3} className={classes.footerModal}>
                  <Grid item xs={6}>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.btnModal}
                      startIcon={<DeleteIcon />}
                      onClick={deleteSocio}
                    >
                      Eliminar
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      variant="contained"
                      onClick={abrirCerrarModalEliminar}
                      className={classes.btnModal}
                    >
                      Cancelar
                    </Button>
                  </Grid>
                </Grid>
              </div>
            </Modal>

            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={abrirModalPdf}
              onClose={abrirCerrarModalPdf}
              BackdropProps={{
                timeout: 500,
              }}
            >
              <div
                style={{
                  display: "block",
                  background: "#fff",
                  padding: "10px 10px",
                }}
              >
                <MyDocument
                  nombre={formik.values.nameLastNameSocio}
                  importe={formik.values.importe}
                  concepto={formik.values.periodo}
                  fecha={fechaFactura}
                />
              </div>
            </Modal>
          </Grid>
        </div>
      </ResponsiveDrawer>
      <Footer />
    </>
  )
}

export default Socios
