import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: "100%",
      },
    },
    rootCard: {
      marginTop: "50px",
      background: "#385040",
      "& > h2": {
        color: "#fff",
      },
    },
    rootSearch: {
      marginTop: "50px",
      boxShadow: "1px 1px 4px 1px #ddd",
    },
    principal: {
      display: "flex",
      justifyContent: "center",
      minHeight: "500px",
      marginBottom: "80px",
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      width: "90%",
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    gridHeader: {
      background: "#ffcc00a8",
      borderRadius: "3px",
      width: "100%",
      height: "70px",
      lineHeight: "70px",
      marginTop: "-55px",
      "& > h2": {
        color: "#000",
      },
    },
    formOk: {
      marginTop: "15px",
      marginLeft: "8px",
      display: "flex",
      justifyContent: "center",
      color: "white",
      background: "green",
      textAlign: "center",
      borderRadius: "5px",
    },
    tableContainer: {
      marginTop: "80px",
      // "& .MuiTable-root tr > *:nth-child(5)": { display: "none" },
    },
    input: {
      height: "40px",
      padding: "10px",
      borderRadius: "8px",
      border: "none",
      boxShadow: "1px 1px 3.8px #0c1d2e",
      marginTop: "10px",
      marginBottom: "14px",
    },
    paperModal: {
      borderRadius: "8px",
      width: "40%",
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      "& > h2": {
        textAlign: "center",
      },
    },
    lblForm: {
      textAlign: "start",
    },
    container: {
      width: "100%",
      flexGrow: 1,
      minHeight: "500px",
    },
    footerModal: {
      marginTop: "45px",
      textAlign: "center",
    },
    btnModal: {
      width: "80%",
    },
    btnAddPayment: {
      margin: theme.spacing(2),
      padding: "18px",
      width: "80%",
      background: "#28a745",
      color: "#fff",
      "&:hover": {
        background: "#218838",
      },
    },
    btnAddNotPayment: {
      margin: theme.spacing(2),
      padding: "18px",
      width: "80%",
      background: "#bd2130",
      color: "#fff",
      "&:hover": {
        background: "#c82333",
      },
    },
    tabs: {
      background: "#036524cf",
      "& > div": {
        "& > span": {
          backgroundColor: "#ffdd57",
          borderBottom: "1px solid #000",
          paddingBottom: "4px",
        },
      },
    },
    containerEliminar: {
      background: "#fff",
      padding: "24px",
      width: "auto",
      minWidth: "600px",
      borderRadius: "5px",
    },
    filterDate: {
      width: "90%",
    },
    filterCard: {
      display: "flex",
      alignItems: "center",
      margin: "20px 0",
    },
    btnSearch: {
      width: "80%",
      height: "50px",
    },
    labelSaldo: {
      fontSize: "25px",
      color: "#5f5f5f",
    },
    btnTable: {
      margin: "7px",
      height: "65%",
      justifyContent: "end",
    },
    sendRta: {
      justifyContent: "center",
    },
    cardMoney: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "120px",
      background: "beige",
    },
    iconMoney: {
      width: "60px",
      height: "60px",
    },
    dataMoney: {
      margin: "auto",
      fontSize: "25px",
    },
  })
)
