export const columnsIngresos = [
  {
    name: "Título",
    selector: (row) => row.titulo_mov,
    sortable: true,
  },
  {
    name: "Monto",
    selector: (row) => row.monto_mov,
    sortable: true,
  },
  {
    name: "Fecha",
    selector: (row) => row.fecha_mov,
    sortable: true,
  },
  {
    name: "Motivo",
    selector: (row) => row.motivo_mov,
    sortable: true,
  },
  {
    name: "Operación",
    selector: (row) => row.operacion_mov,
    sortable: true,
  },
]

const handleClick = (title) => {
  console.log(title)
}

export const columnsEgreso = [
  {
    name: "Título",
    selector: (row) => row.titulo_mov,
    sortable: true,
  },
  {
    name: "Monto",
    selector: (row) => row.monto_mov,
    sortable: true,
  },
  {
    name: "Fecha",
    selector: (row) => row.fecha_mov,
    sortable: true,
  },
  {
    name: "Motivo",
    selector: (row) => row.motivo_mov,
    sortable: true,
  },
  {
    name: "Operación",
    selector: (row) => row.operacion_mov,
    sortable: true,
  },
  {
    name: "Action",
    sortable: false,
    selector: "null",
    cell: (d) => [
      // eslint-disable-next-line react/jsx-filename-extension
      <button
        key={1}
        type="button"
        onClick={handleClick.bind(this, d.titulo_mov)}
      >
        Editar
      </button>,
      <button
        key={2}
        type="button"
        className="btnDanger"
        onClick={handleClick.bind(this, d.titulo_mov)}
      >
        Eliminar
      </button>,
    ],
  },
]

export const data = [
  {
    id_mov: 3,
    titulo_mov: "Actu Egresooo nuevo",
    monto_mov: "100",
    fecha_mov: "12/09/2021",
    motivo_mov: "Cuotas Liga Fútbol",
    operacion_mov: "Egreso",
  },
  {
    id_mov: 6,
    titulo_mov: "egreso modif",
    monto_mov: "350",
    fecha_mov: "11/09/2021",
    motivo_mov: "Gastos Generales",
    operacion_mov: "Egreso",
  },
]
