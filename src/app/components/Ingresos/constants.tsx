export const data = [
  {
    title: "Ingreso cuota ",
    cant: "150.00",
    date: "23/08/2021",
    motivo: "Depósitos Bancarios",
    Idoperation: 1,
    operation: "Egreso",
  },
  {
    title: "Ingreso nuevo socio",
    cant: "150.00",
    date: "23/08/2021",
    motivo: "Alquileres",
    Idoperation: 0,
    operation: "Ingreso",
  },
  {
    title: "Ingreso cuota socio",
    cant: "150.00",
    date: "23/08/2021",
    motivo: "Depósitos Bancarios",
    Idoperation: 1,
    operation: "Egreso",
  },
  {
    title: "Ingreso cuota socio",
    cant: "150.00",
    date: "23/08/2021",
    motivo: "Depósitos Bancarios",
    Idoperation: 1,
    operation: "Egreso",
  },
  {
    title: "Ingreso cuota socio",
    cant: "150.00",
    date: "23/08/2021",
    motivo: "Alquileres",
    Idoperation: 0,
    operation: "Ingreso",
  },
  {
    title: "Ingreso cuota socio",
    cant: "150.00",
    date: "23/08/2021",
    motivo: "Alquileres",
    Idoperation: 0,
    operation: "Ingreso",
  },
  {
    title: "Ingreso cuota socio",
    cant: "150.00",
    date: "23/08/2021",
    motivo: "Depósitos Bancarios",
    Idoperation: 1,
    operation: "Egreso",
  },
]

export const columns = [
  { name: "Título", selector: (row: any) => row.titulo_mov },
  { name: "Monto", selector: (row: any) => row.monto_mov },
  { name: "Fecha", selector: (row: any) => row.fecha_mov },
  { name: "Motivo", selector: (row: any) => row.motivo_mov },
  { name: "Operación", selector: (row: any) => row.operacion_mov },
]

export const listIngreso = [
  { id: "1", name: "Cuotas Sociales", selected: "selected" },
  { id: "2", name: "Alquileres", selected: "" },
  { id: "3", name: "Donaciones", selected: "" },
  { id: "4", name: "Rifas y Eventos", selected: "" },
  { id: "5", name: "Subsidios", selected: "" },
]

export const listEgreso = [
  { id: "1", name: "Seguros", selected: "selected" },
  { id: "2", name: "Cuotas Liga Fútbol", selected: "" },
  { id: "3", name: "Gastos Generales", selected: "" },
  { id: "4", name: "Depósitos Bancarios", selected: "" },
]
