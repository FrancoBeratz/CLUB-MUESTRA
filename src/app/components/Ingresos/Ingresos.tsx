import {
  AppBar,
  Grid,
  InputLabel,
  Tab,
  Tabs,
  TextField,
} from "@material-ui/core"
import React, { useState, useEffect } from "react"
import Paper from "@material-ui/core/Paper"
import Modal from "@material-ui/core/Modal"
import Button from "@material-ui/core/Button"
import { useFormik } from "formik"
import Card from "@material-ui/core/Card"
import LocalAtmIcon from "@material-ui/icons/LocalAtm"
import MoneyOffIcon from "@material-ui/icons/MoneyOff"
import { useStyles } from "./styles"
import DeleteIcon from "@material-ui/icons/Delete"
import { listIngreso, listEgreso } from "./constants"
import { paginationOption } from "../../../contants"
import SearchIcon from "@material-ui/icons/Search"
import { useDispatch, useSelector, connect } from "react-redux"
import {
  removeMovimiento,
  updateMovimiento,
  addMovimiento,
  getFilterMovimiento,
} from "../../../redux/actions/movimientosAction"
import store from "../../../redux/store"
import DataTable from "react-data-table-component"
import DataTableExtensions from "react-data-table-component-extensions"
import "react-data-table-component-extensions/dist/index.css"
import SortIcon from "@material-ui/icons/ArrowDownward"
import EditIcon from "@material-ui/icons/Edit"
import ResponsiveDrawer from "../ResponsiveDrawer"
import Alert from "@material-ui/lab/Alert"
import CheckIcon from "@material-ui/icons/Check"
import Footer from "../Footer"
import AttachMoneyIcon from "@material-ui/icons/AttachMoney"

export function Ingresos() {
  const classes = useStyles()
  const [abrirModalAlta, setAbrirModalAlta] = useState(false)
  const [ingreso, setIngreso] = useState(false)
  const [egreso, setEgreso] = useState(false)
  const [editar, setEditar] = useState(false)
  const [valueTab, setValueTab] = useState(0)
  const [correctList, setCorrectList] = useState([{}])
  const [abrirModalEliminar, setAbrirModalEliminar] = useState(false)
  const [statusOperation, setStatusOperation] = useState("")
  const [titleOperation, setTitleOperation] = useState("")
  const [sendFromUpdate, setSendFromUpdate] = useState(false)
  const [sendFrom, setSendForm] = useState(false)
  const [idMov, setIdMov] = useState("")
  const dispatch = useDispatch()

  const options = { minimumFractionDigits: 2 }
  type RootState = ReturnType<typeof store.getState>
  const movimientos = useSelector((state: RootState) => state)
  const movimientosIngresos: any[] = movimientos.allMovimientos.ingresos
  const movimientosEgresos: any[] = movimientos.allMovimientos.egresos
  const sumaIngresos: any =
    movimientos.allMovimientos.sumaIngresos.toLocaleString("es-ES", options)
  const sumaEgresos: any =
    movimientos.allMovimientos.sumaEgresos.toLocaleString("es-ES", options)
  const diferenciaSaldos: any =
    movimientos.allMovimientos.diferenciaSaldo.toLocaleString("es-ES", options)

  const columnsTable = [
    {
      name: "Título",
      selector: (row) => row.titulo_mov,
      sortable: true,
    },
    {
      name: "Monto",
      selector: (row) => row.monto_mov,
      sortable: true,
    },
    {
      name: "Fecha",
      selector: (row) => row.fecha_mov,
      sortable: true,
    },
    {
      name: "Motivo",
      selector: (row) => row.motivo_mov,
      sortable: true,
    },
    {
      name: "Operación",
      selector: (row) => row.operacion_mov,
      sortable: true,
    },
    {
      name: "Acción",
      sortable: false,
      selector: "null",
      cell: (d) => [
        <Button
          key={1}
          variant="contained"
          type="button"
          className={classes.btnTable}
          color="primary"
          title="Editar"
          startIcon={<EditIcon />}
          // eslint-disable-next-line @typescript-eslint/no-use-before-define
          onClick={() => seleccionarSocio(d, "Editar")}
        ></Button>,
        <Button
          key={2}
          variant="contained"
          className={classes.btnTable}
          startIcon={<DeleteIcon />}
          type="button"
          color="secondary"
          title="Eliminar"
          // eslint-disable-next-line @typescript-eslint/no-use-before-define
          onClick={() => seleccionarSocio(d, "Eliminar")}
        ></Button>,
      ],
      grow: 1,
      center: true,
    },
  ]

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-use-before-define
    fetchMovimientos()
  }, [])

  const handleChangeTab = (event: any, newValue: number) => {
    setValueTab(newValue)
  }

  const tableDataIngreso = {
    columns: columnsTable,
    data: movimientosIngresos,
    print: false,
    export: false,
    filterPlaceholder: "Ingrese una palabra",
  }

  const tableDataEgreso = {
    columns: columnsTable,
    data: movimientosEgresos,
    print: false,
    export: false,
    filterPlaceholder: "Ingrese una palabra",
  }

  const validate = (values: any) => {
    const errors: any = {}

    if (!values.title) {
      errors.title = "Debe colocar un nombre"
    } else if (values.title.length < 4) {
      errors.title = "Must be 5 characters or more"
    }

    if (!values.lastname) {
      errors.lastname = "Debe colocar un apellido"
    } else if (values.lastname.length < 8) {
      errors.lastname = "Must be 8 characters or more"
    } else if (values.lastname === "12345678") {
      errors.lastname = "Must not be 12345678 !!!"
    }

    if (!values.email) {
      errors.email = "Debe colocar un email"
    } else if (values.email.length < 8) {
      errors.email = "Must be 8 characters or more"
    } else if (values.email === "12345678") {
      errors.email = "Must not be 12345678 !!!"
    }
    return errors
  }

  let fechaObtenida = ""
  const date = new Date()
  const day = date.getDate()
  const month = date.getMonth()
  const year = date.getFullYear()
  if (month < 10 && day < 10) {
    fechaObtenida = `${year}-0${month}-0${day}`
  } else if (month < 10) {
    fechaObtenida = `${year}-0${month}-${day}`
  } else if (day < 10) {
    fechaObtenida = `${year}-${month}-0${day}`
  } else {
    fechaObtenida = `${year}-${month}-${day}`
  }

  const formik = useFormik({
    initialValues: {
      title: "",
      cant: "",
      date: fechaObtenida,
      motivo: "",
      estado: "",
      fechaDesde: "2021-01-01",
      fechaHasta: "2022-12-30",
      nameMovimiento: "Ingreso",
    },
    validate,
    onSubmit: (values: any, { resetForm }) => {
      resetForm()
    },
  })

  function convertDate(texto) {
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, "$3/$2/$1")
  }

  const fetchMovimientos = () => {
    dispatch(getFilterMovimiento(formik.values))
  }

  const limpiarForm = () => {
    formik.values.title = ""
    formik.values.cant = ""
    formik.values.date = ""
    formik.values.motivo = ""
    formik.values.estado = ""
  }

  const abrirCerrarModalIngreso = () => {
    setIngreso(true)
    setEgreso(false)
    setEditar(false)
    limpiarForm()
    formik.values.date = fechaObtenida
    formik.values.estado = "Ingreso"
    formik.values.motivo = "Cuotas Sociales"
    setCorrectList(listIngreso)
    setAbrirModalAlta(!abrirModalAlta)
  }

  const abrirCerrarModalEgreso = () => {
    setEgreso(true)
    setIngreso(false)
    setEditar(false)
    limpiarForm()
    formik.values.date = fechaObtenida
    formik.values.estado = "Egreso"
    formik.values.motivo = "Seguros"
    setCorrectList(listEgreso)
    setAbrirModalAlta(!abrirModalAlta)
  }

  const abrirCerrarModalEditar = () => {
    setEditar(true)
    setEgreso(false)
    setIngreso(false)
    setAbrirModalAlta(!abrirModalAlta)
  }

  const seleccionarSocio = (rowData: any, caso: any) => {
    setIdMov(rowData.id_mov)
    if (caso === "Editar") {
      formik.values.title = rowData.titulo_mov
      formik.values.cant = rowData.monto_mov.replace(",", ".")
      formik.values.motivo = rowData.motivo_mov
      const [year, month, day] = rowData.fecha_mov
        ? rowData.fecha_mov.split("/")
        : ""
      formik.values.date = `${day}-${month}-${year}`
      if (rowData.operacion_mov === "Ingreso") {
        setCorrectList(listIngreso)
      } else {
        setCorrectList(listEgreso)
      }
      abrirCerrarModalEditar()
    } else if (caso === "Eliminar") {
      setStatusOperation(rowData.operacion_mov)
      setTitleOperation(rowData.titulo_mov)
      setAbrirModalEliminar(!abrirModalEliminar)
    }
  }

  const abrirCerrarModalEliminar = () => {
    setAbrirModalEliminar(!abrirModalEliminar)
  }

  const update = (id: any) => {
    setSendFromUpdate(true)
    setTimeout(() => {
      dispatch(updateMovimiento(id, formik.values))
      setAbrirModalAlta(!abrirModalAlta)
      setSendFromUpdate(false)
    }, 3000)
    dispatch(getFilterMovimiento(formik.values))
  }

  const deleteMovimiento = () => {
    dispatch(removeMovimiento(idMov))
    abrirCerrarModalEliminar()
    dispatch(getFilterMovimiento(formik.values))
  }

  const altaEgreso = (formik: any) => {
    setSendForm(true)
    setTimeout(() => {
      setSendForm(false)
      dispatch(addMovimiento(formik))
      setAbrirModalAlta(!abrirModalAlta)
    }, 3000)
  }

  const altaIngreso = (formik: any) => {
    setSendForm(true)
    setTimeout(() => {
      setSendForm(false)
      dispatch(addMovimiento(formik))
      setAbrirModalAlta(!abrirModalAlta)
    }, 3000)
  }

  const searchMovimiento = (e: any) => {
    console.log("value", valueTab)
    dispatch(getFilterMovimiento(formik.values))
  }

  const listAllMov = () => {
    formik.values.fechaDesde = "2021-01-01"
    formik.values.fechaHasta = "2022-12-30"
    dispatch(getFilterMovimiento(formik.values))
  }

  const bodyAlta = (
    <div className={classes.paperModal}>
      {ingreso && <h2>Ingreso</h2>}
      {egreso && <h2>Egreso</h2>}
      {editar && <h2>Editar</h2>}
      <form onSubmit={formik.handleSubmit} className={classes.root}>
        <TextField
          id="standard-basic"
          label="Título"
          value={formik.values.title}
          name="title"
          onChange={formik.handleChange}
          variant="outlined"
        />
        <TextField
          id="standard-basic"
          label="Monto"
          placeholder="1000.00"
          value={formik.values.cant.replace(",", ".")}
          name="cant"
          onChange={formik.handleChange}
          variant="outlined"
          type="text"
        />
        <TextField
          id="standard-basic"
          label="Fecha"
          value={formik.values.date}
          name="date"
          onChange={formik.handleChange}
          variant="outlined"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="optionIngreso"
          select
          label="Motivo"
          name="motivo"
          value={formik.values.motivo}
          onChange={formik.handleChange}
          SelectProps={{
            native: true,
          }}
          variant="outlined"
        >
          {correctList.map((ingreso: any, index: any) => {
            return (
              <option value={ingreso.name} key={index}>
                {ingreso.name}
              </option>
            )
          })}
        </TextField>
        <Grid container spacing={3} className={classes.footerModal}>
          <Grid item xs={6}>
            {ingreso && (
              <Button
                variant="contained"
                type="button"
                color="primary"
                className={classes.btnModal}
                onClick={() => altaIngreso(formik.values)}
              >
                Guardar Ingreso
              </Button>
            )}
            {egreso && (
              <Button
                variant="contained"
                type="button"
                color="primary"
                className={classes.btnModal}
                onClick={() => altaEgreso(formik.values)}
              >
                Guardar Egreso
              </Button>
            )}
            {editar && (
              <Button
                variant="contained"
                type="button"
                color="primary"
                className={classes.btnModal}
                onClick={() => update({ idMov })}
              >
                Actualizar
              </Button>
            )}
          </Grid>
          <Grid item xs={6}>
            <Button
              variant="contained"
              onClick={abrirCerrarModalIngreso}
              className={classes.btnModal}
            >
              Cancelar
            </Button>
          </Grid>
        </Grid>
        {sendFrom && (
          <Grid container className={classes.formOk}>
            <Grid item xs={12}>
              <Alert
                variant="filled"
                icon={<CheckIcon fontSize="inherit" />}
                severity="success"
                className={classes.sendRta}
              >
                Guardado Correctamente
              </Alert>
            </Grid>
          </Grid>
        )}
        {sendFromUpdate && (
          <Grid container className={classes.formOk}>
            <Grid item xs={12}>
              <Alert
                variant="filled"
                icon={<CheckIcon fontSize="inherit" />}
                severity="success"
                className={classes.sendRta}
              >
                Actualizado correctamente
              </Alert>
            </Grid>
          </Grid>
        )}
      </form>
    </div>
  )

  return (
    <>
      <ResponsiveDrawer>
        <div>
          <Grid container spacing={3}>
            <Grid item xs={12} className={classes.principal}>
              <Paper className={classes.paper}>
                <div className={classes.gridHeader}>
                  <h2>Ingresos y Egresos del club</h2>
                </div>
                <div>
                  <Card className={classes.rootCard}>
                    <h2>Carga de Ingresos/Egresos</h2>
                    <Grid container spacing={2}>
                      <Grid item xs={6}>
                        <Button
                          variant="contained"
                          className={classes.btnAddPayment}
                          onClick={abrirCerrarModalIngreso}
                          startIcon={<LocalAtmIcon />}
                        >
                          Carga de Ingresos
                        </Button>
                      </Grid>
                      <Grid item xs={6}>
                        <Button
                          variant="contained"
                          className={classes.btnAddNotPayment}
                          startIcon={<MoneyOffIcon />}
                          onClick={abrirCerrarModalEgreso}
                        >
                          Carga de Egresos
                        </Button>
                      </Grid>
                    </Grid>
                  </Card>
                </div>
                <div>
                  <Card className={classes.rootSearch}>
                    <h2>Filtros de búsqueda</h2>
                    <form onSubmit={formik.handleSubmit} action="">
                      <Grid
                        container
                        spacing={1}
                        className={classes.filterCard}
                      >
                        <Grid item lg={3} md={3} sm={6}>
                          <TextField
                            id="standard-basic"
                            label="Fecha Desde"
                            className={classes.filterDate}
                            name="fechaDesde"
                            value={formik.values.fechaDesde}
                            onChange={formik.handleChange}
                            variant="outlined"
                            type="date"
                            InputLabelProps={{
                              shrink: true,
                            }}
                          />
                        </Grid>
                        <Grid item lg={3} md={3} sm={6}>
                          <TextField
                            id="standard-basic"
                            label="Fecha Hasta"
                            className={classes.filterDate}
                            name="fechaHasta"
                            value={formik.values.fechaHasta}
                            onChange={formik.handleChange}
                            variant="outlined"
                            type="date"
                            InputLabelProps={{
                              shrink: true,
                            }}
                          />
                        </Grid>
                        <Grid item lg={3} md={3} sm={12}>
                          <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            className={classes.btnSearch}
                            startIcon={<SearchIcon />}
                            onClick={searchMovimiento}
                          >
                            Buscar
                          </Button>
                        </Grid>
                        <Grid item lg={3} md={3} sm={12}>
                          <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            className={classes.btnSearch}
                            startIcon={<SearchIcon />}
                            onClick={listAllMov}
                          >
                            Listar Todos
                          </Button>
                        </Grid>
                      </Grid>
                    </form>
                  </Card>
                </div>
                <div className={classes.tableContainer}>
                  <AppBar position="static">
                    <Tabs
                      variant="fullWidth"
                      value={valueTab}
                      // onChange={handleChangeTab}
                      aria-label="nav tabs example"
                      className={classes.tabs}
                    >
                      <Tab
                        label="Listado de Ingresos"
                        onClick={() => handleChangeTab("", 0)}
                      />
                      <Tab
                        label="Listado de Egresos"
                        onClick={() => handleChangeTab("", 1)}
                      />
                    </Tabs>
                  </AppBar>
                  {valueTab === 0 ? (
                    <DataTableExtensions {...tableDataIngreso}>
                      <DataTable
                        columns={columnsTable}
                        data={movimientosIngresos}
                        sortIcon={<SortIcon />}
                        defaultSortAsc={true}
                        title="Listado de Ingresos"
                        pagination
                        highlightOnHover
                        striped={true}
                        dense
                        paginationComponentOptions={paginationOption}
                        noDataComponent="No hay datos para mostrar"
                      />
                    </DataTableExtensions>
                  ) : (
                    <DataTableExtensions {...tableDataEgreso}>
                      <DataTable
                        columns={columnsTable}
                        data={movimientosEgresos}
                        sortIcon={<SortIcon />}
                        title="Listado de Egresos"
                        pagination
                        highlightOnHover
                        striped={true}
                        dense
                        paginationComponentOptions={paginationOption}
                        noDataComponent="No hay datos para mostrar"
                      />
                    </DataTableExtensions>
                  )}
                </div>

                <div>
                  <Card className={classes.rootSearch}>
                    <h2>
                      Saldos desde {convertDate(formik.values.fechaDesde)} hasta{" "}
                      {convertDate(formik.values.fechaHasta)}
                    </h2>
                    <Grid container spacing={4}>
                      <Grid item lg={4} md={4} xs={4}>
                        <Card className={classes.cardMoney} variant="outlined">
                          <Grid container>
                            <Grid item lg={2} md={2} xs={2}>
                              <AttachMoneyIcon className={classes.iconMoney} />
                            </Grid>
                            <Grid
                              item
                              lg={10}
                              md={10}
                              xs={10}
                              className={classes.dataMoney}
                            >
                              <InputLabel className={classes.labelSaldo}>
                                Total saldo de ingresos: ${sumaIngresos}
                              </InputLabel>
                            </Grid>
                          </Grid>
                        </Card>
                      </Grid>
                      <Grid item lg={4} md={4} xs={4}>
                        <Card className={classes.cardMoney} variant="outlined">
                          <Grid container>
                            <Grid item lg={2} md={2} xs={2}>
                              <AttachMoneyIcon className={classes.iconMoney} />
                            </Grid>
                            <Grid
                              item
                              lg={10}
                              md={10}
                              xs={10}
                              className={classes.dataMoney}
                            >
                              <InputLabel className={classes.labelSaldo}>
                                Total saldo de Egresos: $ {sumaEgresos}
                              </InputLabel>
                            </Grid>
                          </Grid>
                        </Card>
                      </Grid>
                      <Grid item lg={4} md={4} xs={4}>
                        <Card className={classes.cardMoney} variant="outlined">
                          <Grid container>
                            <Grid item lg={2} md={2} xs={2}>
                              <AttachMoneyIcon className={classes.iconMoney} />
                            </Grid>
                            <Grid
                              item
                              lg={10}
                              md={10}
                              xs={10}
                              className={classes.dataMoney}
                            >
                              <InputLabel className={classes.labelSaldo}>
                                Diferencia de saldos: $ {diferenciaSaldos}
                              </InputLabel>
                            </Grid>
                          </Grid>
                        </Card>
                      </Grid>
                    </Grid>
                  </Card>
                </div>
              </Paper>
            </Grid>

            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={abrirModalAlta}
              onClose={abrirCerrarModalEditar}
              BackdropProps={{
                timeout: 500,
              }}
            >
              {bodyAlta}
            </Modal>

            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={abrirModalAlta}
              onClose={abrirCerrarModalIngreso}
              BackdropProps={{
                timeout: 500,
              }}
            >
              {bodyAlta}
            </Modal>

            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={abrirModalEliminar}
              onClose={abrirCerrarModalEliminar}
              BackdropProps={{
                timeout: 500,
              }}
            >
              <div className={classes.containerEliminar}>
                <h2>
                  ¿Desea eliminar el siguiente {statusOperation}: &quot;
                  {titleOperation}
                  &quot;?
                </h2>
                <Grid container spacing={3} className={classes.footerModal}>
                  <Grid item xs={6}>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.btnModal}
                      startIcon={<DeleteIcon />}
                      onClick={deleteMovimiento}
                    >
                      Eliminar
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      variant="contained"
                      onClick={abrirCerrarModalEliminar}
                      className={classes.btnModal}
                    >
                      Cancelar
                    </Button>
                  </Grid>
                </Grid>
              </div>
            </Modal>
          </Grid>
        </div>
      </ResponsiveDrawer>
      <Footer />
    </>
  )
}

// export default Ingresos
export default connect()(Ingresos)
