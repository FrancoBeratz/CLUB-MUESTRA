import React from "react"
import { useStyles } from "./styles"

const index = () => {
  const classes = useStyles()

  return (
    <div className={classes.container}>
      <p className={classes.copyright}>
        Copyright ©2021 Todos los derechos reservados por{" "}
        <a
          className={classes.linkIddeaDevs}
          href="https://iddeadevs21.github.io/"
          target="_blank"
          rel="noreferrer"
        >
          IddeaDevs
        </a>
      </p>
    </div>
  )
}

export default index
