import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      height: "50px",
      background: "#e9e9e9",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
      position: "absolute",
    },
    copyright: {
      textAlign: "center",
    },
    linkIddeaDevs: {
      textDecoration: "none",
      color: "#000",
      fontWeight: "bold",
    },
  })
)
