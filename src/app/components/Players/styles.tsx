import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: "95%",
      },
      "& .MuiGrid-container": {
        width: "97%",
      },
      "& .css-2b097c-container": { width: "95%" },
      overflowY: "scroll",
      "&::-webkit-scrollbar": { display: "none" },
    },
    paper: {
      width: "90%",
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    paperModal: {
      borderRadius: "8px",
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      "& > h2": {
        textAlign: "center",
      },
      [theme.breakpoints.up("lg")]: {
        width: "80%",
      },
      [theme.breakpoints.up("md")]: {
        width: "40%",
      },
    },
    principal: {
      display: "flex",
      justifyContent: "center",
      minHeight: "500px",
    },
    control: {
      padding: theme.spacing(2),
    },
    form: {
      border: "1px solid #ddd",
      width: "100%",
      borderRadius: "10px",
      padding: "50px",
      background: "#fff",
    },
    title: {
      textAlign: "center",
      borderBottom: "2px solid #ddd",
    },
    lblForm: {
      textAlign: "start",
    },
    btnAlta: {
      height: "46px",
      marginTop: "14px",
      backgroundColor: "#009933",
      color: "#fff",
      borderRadius: "8px",
      border: "1px solid rgb(8, 68, 6)",
      boxShadow: "1px 2px 3px #252525aa",
      fontSize: "15px",
    },
    containerTable: {
      marginTop: "15px",
      justifyContent: "center",
    },
    containerform: {
      display: "flex",
      justifyContent: "center",
      width: "100%",
    },
    container: {
      width: "100%",
      flexGrow: 1,
      minHeight: "500px",
    },
    tableFlex: {
      width: "80%",
      background: "#fff",
    },
    table: {
      minWidth: 650,
    },
    tableContainer: {
      borderRadius: 15,
      width: "80%",
      border: "1px solid #000",
    },
    tableHeaderCell: {
      fontWeight: "bold",
      backgroundColor: "#ffcc0066",
      color: "#000",
      borderBottom: "1px solid #000",
    },
    avatar: {
      backgroundColor: theme.palette.primary.light,
      color: theme.palette.getContrastText(theme.palette.primary.light),
    },
    name: {
      fontWeight: "bold",
      color: theme.palette.secondary.dark,
    },
    status: {
      fontWeight: "bold",
      fontSize: "0.75rem",
      color: "white",
      backgroundColor: "grey",
      borderRadius: 8,
      padding: "3px 10px",
      display: "inline-block",
    },
    gridHeader: {
      background: "#036524a6",
      margin: "-35px 15px 0",
      borderRadius: "3px",
      padding: "5px",
      "& > h2": {
        color: "#fff",
      },
    },
    btnAddUser: {
      padding: "18px",
      width: "30%",
    },
    btnAllPlayers: {
      padding: "18px",
      width: "85%",
    },
    dialogContainer: {
      width: "100%",
      background: "blue",
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    footerModal: {
      marginTop: "45px",
      textAlign: "center",
    },
    btnModal: {
      width: "80%",
    },
    input: {
      height: "40px",
      padding: "10px",
      borderRadius: "8px",
      border: "none",
      boxShadow: "1px 1px 3.8px #0c1d2e",
      marginTop: "10px",
      marginBottom: "14px",
    },
    division: {
      display: "flex",
      justifyContent: "flex-start",
      marginBottom: "45px",
      marginTop: "40px",
      marginLeft: "30px",
      "& > label": {
        fontSize: "25px",
        color: "#000",
        fontWeight: "bold",
        marginRight: "15px",
      },
      "& > select": {
        borderRadius: "5px",
        width: "30%",
        paddingLeft: "10px",
        marginRight: "10px",
        fontSize: "18px",
      },
    },
    btnSearch: {
      width: "30%",
    },
    contBtnUser: {
      justifyContent: "center",
      display: " flex",
      alignItems: "center",
      marginTop: "25px",
    },
    formOk: {
      marginTop: "15px",
      marginLeft: "8px",
      display: "flex",
      justifyContent: "center",
      color: "white",
      background: "green",
      textAlign: "center",
      borderRadius: "5px",
    },
    containerEliminar: {
      background: "#fff",
      padding: "24px",
      width: "auto",
      minWidth: "600px",
      borderRadius: "5px",
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
      maxWidth: 300,
    },
    chips: {
      display: "flex",
      flexWrap: "wrap",
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
    selectionSelect: {
      marginTop: theme.spacing(1),
      marginLeft: "8px",
      width: "100%",
      "& > .css-yk16xz-control": {
        height: "50px",
      },
    },
    rootCard: {
      flexGrow: 1,
      margin: "50px 0px",
      background: "#fff",
      boxShadow: "0px 0px 2px black",
    },
    btnTable: {
      margin: "7px",
      height: "65%",
      justifyContent: "end",
    },
    sendRta: {
      justifyContent: "center",
    },
  })
)
