import { useDispatch } from "react-redux"
import axios from "axios"
import { setPlayers } from "../../../redux/actions/playersActions"

export const fetchPlayers = async () => {
  const dispatch = useDispatch()
  const response = await axios
    .get("http://localhost:3001/allPlayers")
    .catch((err: any) => {
      console.log("Err", err)
    })
  console.log("response", response)
  dispatch(setPlayers(response))
}
