import React, { useState, useEffect } from "react"
import { useFormik } from "formik"
import { Card, TextField } from "@material-ui/core"
import "../../../App.css"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import AddCircleIcon from "@material-ui/icons/AddCircle"
import Modal from "@material-ui/core/Modal"
import SortIcon from "@material-ui/icons/ArrowDownward"
import Select from "react-select"
import makeAnimated from "react-select/animated"
import { useStyles } from "./styles"
import DeleteIcon from "@material-ui/icons/Delete"
import { useDispatch, useSelector } from "react-redux"
import store from "../../../redux/store"
import GroupIcon from "@material-ui/icons/Group"
import {
  addPlayer,
  fetchPlayers,
  removePlayer,
  updatePlayer,
} from "../../../redux/actions/playersActions"
import { paginationOption, selectPlayer } from "../../../contants"
import DataTable from "react-data-table-component"
import DataTableExtensions from "react-data-table-component-extensions"
import EditIcon from "@material-ui/icons/Edit"
import ResponsiveDrawer from "../ResponsiveDrawer"
import Alert from "@material-ui/lab/Alert"
import CheckIcon from "@material-ui/icons/Check"
import Footer from "../Footer"

export function Players() {
  const [sendFrom, setSendForm] = useState(false)
  const [sendFromUpdate, setSendFromUpdate] = useState(false)
  const [abrirModalAlta, setAbrirModalAlta] = useState(false)
  const [abrirModalEliminar, setAbrirModalEliminar] = useState(false)
  const [titleEdit, setTitleEdit] = useState(false)
  const [titleAlta, setTitleAlta] = useState(false)
  const [jugador, setJugador] = useState("")
  const [idJugador, setIdJugador] = useState("")
  const [valueSelect, setValueSelect] = useState([])
  const [valoresSelectEdit, setValoresSelectEdit] = useState([{}])
  const classes = useStyles()
  const dispatch = useDispatch()
  type RootState = ReturnType<typeof store.getState>
  const player = useSelector((state: RootState) => state)
  const data: any[] = player.allPlayers.players

  const formatFecha = (fecha: any) => {
    const [year, month, day] = fecha ? fecha.split("/") : ""
    const fechaNacimiento = `${day}-${month}-${year}`
    return fechaNacimiento
  }
  console.log("newplayer", data)

  useEffect(() => {
    const allPlayers = "all"
    dispatch(fetchPlayers(allPlayers))
  }, [dispatch])

  const validate = (values: any) => {
    const errors: any = {}
    if (!values.nameAlta) {
      errors.nameAlta = "Debe ingresar un nombre"
    }
    if (!values.dniAlta) {
      errors.dniAlta = "Debe ingresar un dni valido"
    }
    if (!values.fechaNacAlta) {
      errors.fechaNacAlta = "Debe ingresar una fecha de nacimiento"
    }
    return errors
  }

  let fechaObtenida = ""
  const date = new Date()
  const day = date.getDate()
  const month = date.getMonth()
  const year = date.getFullYear()
  if (month < 10 && day < 10) {
    fechaObtenida = `${year}-0${month}-0${day}`
  } else if (month < 10) {
    fechaObtenida = `${year}-0${month}-${day}`
  } else if (day < 10) {
    fechaObtenida = `${year}-${month}-0${day}`
  } else {
    fechaObtenida = `${year}-${month}-${day}`
  }
  console.log("fechaObtenida", fechaObtenida)
  const animatedComponents = makeAnimated()
  const formik = useFormik({
    initialValues: {
      nameAlta: "",
      dniAlta: "",
      telAlta: "",
      fechaNacAlta: "0000-00-00",
      fechaAlta: fechaObtenida,
      fechaBaja: "0000-00-00",
      clubOrigenAlta: "",
      clubDestinoAlta: "",
      division: "Veteranos(FM)",
    },
    validate,
    onSubmit: (values, { resetForm }) => {
      console.log(values)
      resetForm()
      setSendForm(true)
      setTimeout(() => {
        setSendForm(false)
        dispatch(addPlayer(values))
        setAbrirModalAlta(!abrirModalAlta)
      }, 3000)
    },
  })

  const limpiarForm = () => {
    formik.values.nameAlta = ""
    formik.values.dniAlta = ""
    formik.values.telAlta = ""
    formik.values.fechaNacAlta = fechaObtenida
    formik.values.fechaAlta = fechaObtenida
    formik.values.fechaBaja = "0001-01-01"
    formik.values.clubOrigenAlta = ""
    formik.values.clubDestinoAlta = ""
    setValueSelect([])
  }

  const update = (id: any) => {
    console.log("values", formik.values)
    setSendFromUpdate(true)
    setTimeout(() => {
      dispatch(updatePlayer(id, formik.values))
      setAbrirModalAlta(!abrirModalAlta)
      limpiarForm()
      setSendFromUpdate(false)
    }, 3000)
  }

  const abrirCerrarModalAlta = () => {
    limpiarForm()
    setTitleEdit(false)
    setTitleAlta(true)
    setAbrirModalAlta(!abrirModalAlta)
  }

  const abrirCerrarModalEliminar = () => {
    setAbrirModalEliminar(!abrirModalEliminar)
  }

  const changeSelect = (e: any) => {
    console.log("e", e)
    const all: any[] = Array.isArray(e) ? e.map((x) => x.label) : []
    console.log("ll", all.join())
    const divisionSelected: string = all.join()
    formik.values.division = divisionSelected
    console.log("ssss", divisionSelected)
  }

  const seleccionarSocio = (rowData: any, caso: any) => {
    console.log(rowData)
    if (caso === "Editar") {
      formik.values.nameAlta = rowData.name_player
      formik.values.dniAlta = rowData.dni_player
      formik.values.telAlta = rowData.tel_player
      formik.values.fechaNacAlta = formatFecha(rowData.nac_player)
      formik.values.fechaAlta = formatFecha(rowData.alta_player)
      formik.values.fechaBaja = formatFecha(rowData.baja_player)
      formik.values.clubOrigenAlta = rowData.origen_player
      formik.values.clubDestinoAlta = rowData.destino_player
      // setValoresSelectEdit(rowData.division_player)
      console.log("valoresSelectEdit", rowData.division_player)
      const strDivision = rowData.division_player
      let selectData: any = []
      if (strDivision === "") {
        console.log("vacio")
        selectData = []
      } else {
        const arr = strDivision.split(",")
        console.log("valoresSelectarr", arr)
        arr.forEach((x: any, index: any) => {
          console.log("x", x)
          selectData.push({
            value: index,
            label: x,
          })
        })
      }
      console.log("selectData", selectData)
      setValoresSelectEdit(selectData)
      setTitleEdit(true)
      setTitleAlta(false)
      setIdJugador(rowData.id_player)
      setAbrirModalAlta(!abrirModalAlta)
    }
    if (caso === "Eliminar") {
      setJugador(rowData.name_player)
      setIdJugador(rowData.id_player)
      setAbrirModalEliminar(!abrirModalEliminar)
    }
  }

  const deletePlayer = () => {
    dispatch(removePlayer(idJugador))
    abrirCerrarModalEliminar()
  }

  const searchDivision = (value: string) => {
    dispatch(fetchPlayers(value))
  }

  const columns = [
    {
      name: "Nombre y Apellido",
      selector: (row) => row.name_player,
      sortable: true,
    },
    {
      name: "Dni",
      selector: (row) => row.dni_player,
      sortable: true,
    },
    {
      name: "Teléfono",
      selector: (row) => row.tel_player,
      sortable: true,
    },
    {
      name: "Fecha Nacimiento",
      selector: (row) => row.nac_player,
      sortable: true,
    },
    {
      name: "Fecha Alta",
      selector: (row) => row.alta_player,
      sortable: true,
    },
    {
      name: "Fecha Baja",
      selector: (row) => row.baja_player,
      sortable: true,
    },
    {
      name: "Club Origen",
      selector: (row) => row.origen_player,
      sortable: true,
    },
    {
      name: "Club Destino",
      selector: (row) => row.destino_player,
      sortable: true,
    },
    {
      name: "Categoria",
      selector: (row) => row.division_player,
      sortable: true,
      grow: 2,
    },
    {
      name: "Acción",
      sortable: false,
      selector: "null",
      cell: (d) => [
        <Button
          key={1}
          variant="contained"
          type="button"
          className={classes.btnTable}
          title="Editar Jugador"
          startIcon={<EditIcon />}
          color="primary"
          onClick={() => seleccionarSocio(d, "Editar")}
        ></Button>,
        <Button
          key={2}
          variant="contained"
          className={classes.btnTable}
          type="button"
          color="secondary"
          title="Eliminar Jugador"
          startIcon={<DeleteIcon />}
          onClick={() => seleccionarSocio(d, "Eliminar")}
        ></Button>,
      ],
      grow: 2,
      center: true,
    },
  ]

  const tableDataPlayers = {
    columns,
    data,
    print: false,
    export: false,
    filterPlaceholder: "Ingrese una palabra",
  }

  const bodyAltaSocio = (
    <div className={classes.paperModal}>
      {titleEdit && <h2>Editar Jugador</h2>}
      {titleAlta && <h2>Alta Nuevo Jugador</h2>}
      <form onSubmit={formik.handleSubmit} className={classes.root}>
        <TextField
          id="standard-basic"
          label="Nombre y Apellido"
          name="nameAlta"
          onChange={formik.handleChange}
          value={formik.values.nameAlta}
          onBlur={formik.handleBlur}
          variant="outlined"
        />
        {formik.errors.nameAlta && formik.touched.nameAlta && (
          <div className="error">{formik.errors.nameAlta}</div>
        )}
        <Grid container>
          <Grid item xs={6}>
            <TextField
              id="standard-basic"
              label="Dni"
              value={formik.values.dniAlta}
              name="dniAlta"
              type="number"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              variant="outlined"
            />
            {formik.errors.dniAlta && formik.touched.dniAlta && (
              <div className="error">{formik.errors.dniAlta}</div>
            )}
          </Grid>
          <Grid item xs={6}>
            <TextField
              id="standard-basic"
              label="Teléfono"
              value={formik.values.telAlta}
              name="telAlta"
              type="number"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              variant="outlined"
            />
          </Grid>
        </Grid>
        <TextField
          id="standard-basic"
          label="Fecha Nacimiento"
          name="fechaNacAlta"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.fechaNacAlta}
          variant="outlined"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
        />
        {formik.errors.fechaNacAlta && formik.touched.fechaNacAlta && (
          <div className="error errorForm">{formik.errors.fechaNacAlta}</div>
        )}
        <Grid container>
          <Grid item xs={6}>
            <TextField
              id="standard-basic"
              label="Fecha Alta"
              name="fechaAlta"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              variant="outlined"
              value={formik.values.fechaAlta}
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              id="standard-basic"
              label="Fecha Baja"
              name="fechaBaja"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.fechaBaja}
              variant="outlined"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={6}>
            <TextField
              id="standard-basic"
              label="Club de Origen"
              value={formik.values.clubOrigenAlta}
              name="clubOrigenAlta"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              id="standard-basic"
              label="Club de Destino"
              value={formik.values.clubDestinoAlta}
              name="clubDestinoAlta"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              variant="outlined"
            />
          </Grid>
        </Grid>
        {titleEdit ? (
          <Select
            closeMenuOnSelect={false}
            components={animatedComponents}
            className={classes.selectionSelect}
            isMulti
            options={selectPlayer}
            onChange={changeSelect}
            placeholder="Seleccionar División"
            defaultValue={valoresSelectEdit}
          />
        ) : (
          <Select
            closeMenuOnSelect={false}
            components={animatedComponents}
            className={classes.selectionSelect}
            isMulti
            options={selectPlayer}
            onChange={changeSelect}
            placeholder="Seleccionar División"
          />
        )}
        {valueSelect}
        {titleAlta && (
          <Grid container spacing={3} className={classes.footerModal}>
            <Grid item xs={6}>
              <Button
                variant="contained"
                type="submit"
                color="primary"
                className={classes.btnModal}
              >
                Guardar
              </Button>
            </Grid>
            <Grid item xs={6}>
              <Button
                variant="contained"
                onClick={abrirCerrarModalAlta}
                className={classes.btnModal}
              >
                Cancelar
              </Button>
            </Grid>
          </Grid>
        )}

        {titleEdit && (
          <Grid container spacing={3} className={classes.footerModal}>
            <Grid item xs={6}>
              <Button
                variant="contained"
                color="primary"
                className={classes.btnModal}
                onClick={() => update({ idJugador })}
              >
                Actualizar
              </Button>
            </Grid>
            <Grid item xs={6}>
              <Button
                variant="contained"
                onClick={abrirCerrarModalAlta}
                className={classes.btnModal}
              >
                Cancelar
              </Button>
            </Grid>
          </Grid>
        )}

        {sendFrom && (
          <Grid container className={classes.formOk}>
            <Grid item xs={12}>
              <Alert
                variant="filled"
                icon={<CheckIcon fontSize="inherit" />}
                severity="success"
                className={classes.sendRta}
              >
                Jugador dado de alta con éxito
              </Alert>
            </Grid>
          </Grid>
        )}
        {sendFromUpdate && (
          <Grid container className={classes.formOk}>
            <Grid item xs={12}>
              <Alert
                variant="filled"
                icon={<CheckIcon fontSize="inherit" />}
                severity="success"
                className={classes.sendRta}
              >
                Jugador actualizado correctamente
              </Alert>
            </Grid>
          </Grid>
        )}
      </form>
    </div>
  )

  return (
    <div className={classes.container}>
      <ResponsiveDrawer>
        <div>
          <Grid container spacing={3}>
            <Grid item xs={12} className={classes.principal}>
              <Paper className={classes.paper}>
                <div className={classes.gridHeader}>
                  <h2>Listado de jugadores del club</h2>
                </div>
                <Card className={classes.rootCard}>
                  <Grid container>
                    <Grid item xs={12} className={classes.contBtnUser}>
                      <Button
                        variant="contained"
                        color="primary"
                        className={classes.btnAddUser}
                        onClick={abrirCerrarModalAlta}
                        startIcon={<AddCircleIcon />}
                      >
                        Nuevo Jugador
                      </Button>
                    </Grid>
                    <Grid container>
                      <Grid item xs={12} md={8} lg={8}>
                        <form>
                          <div
                            style={{ width: "100%" }}
                            className={classes.division}
                          >
                            <TextField
                              id="outlined-select-currency-native"
                              select
                              label="Seleccionar division"
                              name="division"
                              onChange={formik.handleChange}
                              style={{ width: "60%", marginRight: "20px" }}
                              value={formik.values.division}
                              SelectProps={{
                                native: true,
                              }}
                              variant="outlined"
                            >
                              {selectPlayer.map((player, index) => {
                                return (
                                  <>
                                    {player.isDisabled ? (
                                      <optgroup
                                        key={player.key}
                                        label={player.value}
                                      ></optgroup>
                                    ) : (
                                      ""
                                    )}
                                    {!player.isDisabled ? (
                                      <option
                                        key={player.key}
                                        value={player.value}
                                      >
                                        {player.value}
                                      </option>
                                    ) : (
                                      ""
                                    )}
                                  </>
                                )
                              })}
                            </TextField>
                            <Button
                              variant="contained"
                              color="primary"
                              onClick={() =>
                                searchDivision(formik.values.division)
                              }
                              className={classes.btnSearch}
                            >
                              Buscar
                            </Button>
                          </div>
                        </form>
                      </Grid>
                      <Grid item xs={12} md={4} lg={4}>
                        <div
                          style={{ width: "100%" }}
                          className={classes.division}
                        >
                          <Button
                            variant="contained"
                            color="primary"
                            className={classes.btnAllPlayers}
                            onClick={() => searchDivision("all")}
                            startIcon={<GroupIcon />}
                          >
                            Listar todos los jugadores
                          </Button>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </Card>
                <div className={classes.containerTable}>
                  <div style={{ width: "100%" }}>
                    <Card style={{ width: "100%" }}>
                      <DataTableExtensions {...tableDataPlayers}>
                        <DataTable
                          columns={columns}
                          data={data}
                          sortIcon={<SortIcon />}
                          defaultSortAsc={true}
                          title="Listado de Jugadores"
                          pagination
                          highlightOnHover
                          dense
                          paginationComponentOptions={paginationOption}
                          noDataComponent="No hay datos para mostrar"
                        />
                      </DataTableExtensions>
                    </Card>
                  </div>
                </div>
              </Paper>
            </Grid>
          </Grid>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={abrirModalAlta}
            onClose={abrirCerrarModalAlta}
            BackdropProps={{
              timeout: 500,
            }}
          >
            {bodyAltaSocio}
          </Modal>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={abrirModalEliminar}
            onClose={abrirCerrarModalEliminar}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <div className={classes.containerEliminar}>
              <h2>¿Desea eliminar a {jugador}?</h2>
              <Grid container spacing={3} className={classes.footerModal}>
                <Grid item xs={6}>
                  <Button
                    variant="contained"
                    color="secondary"
                    className={classes.btnModal}
                    startIcon={<DeleteIcon />}
                    onClick={deletePlayer}
                  >
                    Eliminar
                  </Button>
                </Grid>
                <Grid item xs={6}>
                  <Button
                    variant="contained"
                    onClick={abrirCerrarModalEliminar}
                    className={classes.btnModal}
                  >
                    Cancelar
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Modal>
        </div>
      </ResponsiveDrawer>
      <Footer />
    </div>
  )
}

export default Players
