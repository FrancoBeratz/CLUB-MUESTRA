const palette: Record<string, any> = {
  gray: {
    main: '#65787E',
  },
  grayDark: {
    main: '#314A53',
  },
  primary: {
    main: "#808080",
  },
  secondary: {
    main: '#f57f00',
  },
  white: {
    main: '#ffffff',
  },
}
export default palette
