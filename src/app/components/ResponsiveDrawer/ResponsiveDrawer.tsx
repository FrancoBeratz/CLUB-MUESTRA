import React, { ReactElement } from "react"
import clsx from "clsx"
import { useTheme } from "@material-ui/core/styles"
import Drawer from "@material-ui/core/Drawer"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import List from "@material-ui/core/List"
import CssBaseline from "@material-ui/core/CssBaseline"
import Typography from "@material-ui/core/Typography"
import Divider from "@material-ui/core/Divider"
import IconButton from "@material-ui/core/IconButton"
import MenuIcon from "@material-ui/icons/Menu"
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft"
import ChevronRightIcon from "@material-ui/icons/ChevronRight"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"
// import Link from "@material-ui/core/Link"
import { NavLink } from "react-router-dom"
import SportsSoccerIcon from "@material-ui/icons/SportsSoccer"
import PeopleIcon from "@material-ui/icons/People"
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn"
import { useStyles } from "./styles"
import ClubImg from "../../../../src/app/components/ResponsiveDrawer/club-cju.png"
interface Props {
  window?: () => Window
  children?: ReactElement
}

export function ResponsiveDrawer(props: Props) {
  const { window, children } = props
  const classes = useStyles()
  const theme = useTheme()
  const [open, setOpen] = React.useState(false)

  const handleDrawerOpen = () => {
    setOpen(true)
  }

  const handleDrawerClose = () => {
    setOpen(false)
  }

  const drawer: ReactElement = (
    <div>
      <List>
        <NavLink
          exact
          to="/jugadores"
          activeClassName={classes.active}
          title="Jugadores"
        >
          <ListItem button className={classes.item}>
            <ListItemIcon className={classes.subItem}>
              <SportsSoccerIcon />
            </ListItemIcon>
            <ListItemText color="white" primary={"Jugadores"} />
          </ListItem>
        </NavLink>
        <NavLink
          exact
          to="/socios"
          activeClassName={classes.active}
          title="Socios"
        >
          <ListItem button className={classes.item}>
            <ListItemIcon className={classes.subItem}>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText color="white" primary={"Socios"} />
          </ListItem>
        </NavLink>
        <NavLink
          exact
          to="/ingresos"
          activeClassName={classes.active}
          title="Movimientos"
        >
          <ListItem button className={classes.item}>
            <ListItemIcon className={classes.subItem}>
              <MonetizationOnIcon />
            </ListItemIcon>
            <ListItemText color="white" primary={"Ingresos"} />
          </ListItem>
        </NavLink>
      </List>
    </div>
  )

  const container: any =
    window !== undefined ? () => window().document.body : undefined

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <img
            src={ClubImg}
            className="logo_club"
            alt=""
            style={{ height: "44px", width: "41px", marginRight: "20px" }}
          />

          <Typography variant="h6" noWrap>
            Club Juventud Unida
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Drawer
          variant="permanent"
          container={container}
          anchor={theme.direction === "rtl" ? "right" : "left"}
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "rtl" ? (
                <ChevronRightIcon />
              ) : (
                <ChevronLeftIcon />
              )}
            </IconButton>
          </div>
          <Divider />
          {drawer}
        </Drawer>
      </nav>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <Toolbar />
        {children}
      </main>
    </div>
  )
}

export default ResponsiveDrawer
