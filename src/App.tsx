import * as React from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Ingresos from "./app/components/Ingresos"
import Players from "./app/components/Players"
import Socios from "./app/components/Socios"
import pdf from "./app/components/Socios/pdf"

export const App = (history: any) => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact={true} component={Players} />
        <Route path="/jugadores" exact={true} component={Players} />
        <Route path="/socios" exact={true} component={Socios} />
        <Route path="/ingresos" exact={true} component={Ingresos} />
        <Route path="/pdf" exact={true} component={pdf} />
      </Switch>
    </Router>
  )
}
