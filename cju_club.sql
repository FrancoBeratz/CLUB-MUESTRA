-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-09-2021 a las 23:16:15
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cju_club`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugadores`
--

CREATE TABLE `jugadores` (
  `id_player` int(11) NOT NULL,
  `name_player` varchar(250) NOT NULL,
  `dni_player` varchar(100) NOT NULL,
  `tel_player` varchar(100) NOT NULL,
  `nac_player` date NOT NULL,
  `alta_player` date NOT NULL,
  `baja_player` date NOT NULL,
  `origen_player` varchar(250) NOT NULL,
  `destino_player` varchar(250) NOT NULL,
  `division_player` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `jugadores`
--

INSERT INTO `jugadores` (`id_player`, `name_player`, `dni_player`, `tel_player`, `nac_player`, `alta_player`, `baja_player`, `origen_player`, `destino_player`, `division_player`) VALUES
(2, 'Martin el loco palermo', '12365498', '987654321', '2021-09-16', '2021-09-17', '2021-09-18', 'Gimnasia 1', 'Tiro Federal 1', 'Primera,Reserva,Cuarta'),
(3, 'Juan Roman Riquelme', '12345678', '1234567', '2021-09-01', '2021-09-16', '0000-00-00', 'club origen', 'Club Dossssssss', 'Primera,Cuarta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id_mov` int(11) NOT NULL,
  `titulo_mov` varchar(250) NOT NULL,
  `monto_mov` varchar(100) NOT NULL,
  `fecha_mov` date NOT NULL,
  `motivo_mov` varchar(100) NOT NULL,
  `operacion_mov` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`id_mov`, `titulo_mov`, `monto_mov`, `fecha_mov`, `motivo_mov`, `operacion_mov`) VALUES
(3, 'Actu Egresooo nuevo', '100', '2021-09-12', 'Cuotas Liga Fútbol', 'Egreso'),
(4, 'ingreso modifificado nuevo wddwa wa awawd dawdaw dwa aw  da  dawd awd awdawd aw', '260', '2021-09-12', 'Depósitos Bancarios', 'Ingreso'),
(5, 'Actu nigreso nuevo', '150.5', '2021-09-12', 'Alquileres', 'Ingreso'),
(6, 'egreso modif', '350', '2021-09-11', 'Gastos Generales', 'Egreso'),
(7, 'Actu Egresooo', '100', '2021-09-16', 'Depósitos Bancarios', 'Egreso'),
(11, 'titleeeee', '100', '2021-10-01', 'Rifas y Eventos', 'Ingreso'),
(20, 'ingreso hoy cuota', '12500', '2021-09-16', 'Alquileres', 'Ingreso'),
(21, 'titulo egreso pago', '2500', '2021-09-16', 'Gastos Generales', 'Egreso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id_soc` int(11) NOT NULL,
  `nombre_soc` varchar(250) NOT NULL,
  `dni_soc` varchar(100) NOT NULL,
  `fechaNac_soc` date NOT NULL,
  `fechaAfil_soc` date NOT NULL,
  `cuotaHasta_soc` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id_soc`, `nombre_soc`, `dni_soc`, `fechaNac_soc`, `fechaAfil_soc`, `cuotaHasta_soc`) VALUES
(3, 'Lucas Pettii', '38737154', '2021-09-01', '2021-09-16', '2021-09-16'),
(7, 'Juan edit', '111111', '2021-09-01', '2021-09-17', '2021-10-31'),
(13, 'Socio nuevo editado', '122122122', '2021-09-01', '2021-09-24', '2021-09-16'),
(15, 'Socio Honorario2', '191919112', '2021-09-16', '2021-09-17', '2022-09-16');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `jugadores`
--
ALTER TABLE `jugadores`
  ADD PRIMARY KEY (`id_player`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id_mov`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id_soc`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `jugadores`
--
ALTER TABLE `jugadores`
  MODIFY `id_player` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id_mov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id_soc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
